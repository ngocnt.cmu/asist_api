import os
import json
import socketio
import uvicorn
import redis
import asyncio
import concurrent.futures
from random import random
from hashlib import sha1
from fastapi import FastAPI
from rich.console import Console
from firebase_admin import db, credentials, initialize_app

from random import randrange, randint
from datetime import datetime
from collections import defaultdict

ping_freq, ping_wait = 25, 60
player_limit         = 2
console              = Console()
app                  = FastAPI() # the app is an API --- a REST API
LOCAL                = True

# create a socket.io server 
sio = socketio.AsyncServer(
    async_mode="asgi",
    cors_allowed_origins="*",
    ping_interval=ping_freq,
    ping_timeout=ping_wait
    # async_handlers=True
)


# wrap with ASGI application 
socket_app = socketio.ASGIApp(sio)

# mount the socket_app to "/" of the API
app.mount("/", socket_app)

# REDIS 
if LOCAL:
    r = redis.StrictRedis(host="localhost", db=0, decode_responses=True)
else:
    r = redis.from_url(os.environ.get("REDIS_URL"),decode_responses=True)

# room_id is some random hash
r.set("n_player", 0)
r.set("room_id", datetime.now().strftime("%Y-%m-%d-%H-%M-%S-") + sha1((str(random()) + str(random())).encode("utf8")).hexdigest())
r.set(str(r.get("room_id"))+"_ready_count", 0)


# an abstract class that provides methods to execute calls asynchronously
executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)


# FIREBASE: credential stuff for firebase
cred = credentials.Certificate(
    os.environ.get("GOOGLE_APPLICATION_CREDENTIALS"))
initialize_app(cred, {"databaseURL": os.environ.get("FIREBASE_URL")})

# Returns a database Reference representing the node at the specified path.
ref = db.reference("/")
# db.reference("/").delete() # will empty database and you will have to add config again 



# saves data to FIREBASE database
def save_data(room_id, sid, message):
    ref.child(str(room_id)+"/"+str(sid)).push().set(json.dumps(message))


# temporary-- need to change to redis version later

victimIDtable = {}
# add room to victimIDtable dictionary 
victimIDtable[r.get("room_id")] = set()
# keep track of whether game has ended in rooms
gameTable = {}
earnings = defaultdict(dict)



# experiments 
experiments = [[0,"request_and_respond"], [1,"automatic_teammate_progress"], [2,"msg_auto_update"]]
medkits_needed = { "g":{0:2, 1:1}, "y": {0:1, 1:2}}





# default 
@sio.on("connect")
async def connect_handler(sid, environ):
    room_id = r.get("room_id")
    console.print("CONNECTED "+str(room_id), sid, style="bold magenta")
    # await sio.emit("welcome", {"data": "1. Welcome: You are connected to Cornell Lightweight Testbest ASIST GAME API through websockets."}, room=room_id)

# waits on the to emit start_wait which happens when game is ready to display/start
@sio.on("start_wait")
async def wait_handler(sid, message):
    message["time"] = datetime.now().isoformat()    
    # increase the player index, if another play can join they would have this ... repeat
    
    # player number and room id which is a random sha
    r.incrby("n_player",1)
    n_player = int(r.get("n_player"))
    room_id = r.get("room_id")
    await sio.save_session(sid, {'room_id': room_id})

    # add player to room
    sio.enter_room(sid, room_id)
    
    console.print(
        "Player joining wait. current player count is",
        n_player, style="bold blue"
    )
    console.print(
        "Consenting player ID is :",
        message['prolificID'], style="bold red"
    )
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, room_id, sid, message)
    # sent the room_id and player index ['green',"yellow","ai", "yellow"], 4, {"green" : 4, "yellow": 2}
    await sio.emit("wait_data",
                    {"rm_id": room_id, 
                    "idx": n_player-1, 
                    "player_limit": player_limit, 
                    "experiment": experiments[2]}, room=sid)
    earnings[room_id][n_player-1] = {"prolificID" : message['prolificID'], "ind" : 0}
    earnings[room_id]['team'] = 0

    # once the game has the amount of players it needs AKA the player limit ... start game
    if n_player == player_limit: 
        print("5d. 2ND if statement",    "\n")
        
        console.print(
            "Starting game. player count in waiting room is",
            n_player,
            style="bold blue",
        )

        # set number of med kits equal to total number of victims
        total_medkits = message["config"]["green_victim_count"] + message["config"]["yellow_victim_count"]
        victimIDtable[room_id] = set()
        victimIDtable[room_id+"med_kits"] = [total_medkits,total_medkits] # [medkits left, total medkits]

        game_type_message = {"data": "Start Introduction and Survey!"}
        
        await sio.emit("start_intro_and_survey", game_type_message, room=room_id)
        gameTable[room_id] = True

        # reset the number of players 
        r.set("n_player", 0)

        # set the room_id to new
        r.set("room_id",datetime.now().strftime("%Y-%m-%d-%H-%M-%S-R-") + sha1((str(random()) + str(random())).encode("utf8")).hexdigest())
        room_id = r.get("room_id")
        r.set(str(room_id)+"_ready_count", 0)

        # add room to victimIDtable dictionary 
        victimIDtable[room_id] = set()
        victimIDtable[room_id+"med_kits"] = [total_medkits,total_medkits]



# wait for all participant to be ready to send START_GAME event
@sio.on("participant_ready")
async def player_ready_handler(sid, message):
    session = await sio.get_session(sid)
    try:
        player_room = session['room_id']
    except:
        player_room = message["rm_id"]
    console.print(player_room, "PARTICIPANT READY", sid, style="bold green")
    room_read_count_str = str(player_room)+"_ready_count"
    current_count = int(r.get(room_read_count_str)) + 1

    console.print("PARTICIPANT READY current count: ", current_count, style="bold green")
    console.print("PARTICIPANT READY current limit: ", player_limit, style="bold green")
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)
    if current_count == player_limit:
        # store this in db
        new_message = {"topic" : "mission_start",
                        "key": "mission_start", 
                        "publisher" : "api",
                        "rm_id" : player_room, 
                        "time" : datetime.now().isoformat()}
        await sio.emit("start_game", new_message, room=player_room)
        loop = asyncio.get_event_loop()
        loop.run_in_executor(executor, save_data, player_room, sid, new_message)


    else:
        r.set(room_read_count_str, current_count)
        


@sio.on("player_move")
async def player_movement_handler(sid, message):
    await sio.emit("player_move", message, room=message["rm_id"])


@sio.on("store_position")
async def player_position_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, message["rm_id"], sid, message)

@sio.on("rescue_pressed")
async def rescue_pressed_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    session      = await sio.get_session(sid)
    player_room  = session['room_id']
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)

# sends rescue attempt if close to a victim
@sio.on("rescue_attempt")
async def rescue_attempt_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    session      = await sio.get_session(sid)
    player_room  = session['room_id']

    medkits_left = victimIDtable[player_room +"med_kits"][0]

    '''
        player with ID 0 is green  --> P0 needs 1 medkit to save green and 2 to save yellow
        player with ID 1 is yellow --> P1 needs 2 medkits to save green and 1 to save yellow
    
    '''
    medkits_needed_to_rescue = medkits_needed[message["victim_id"][1]][message["idx"]]

    new_message = message.copy()
    # check if there are enough medkits and that the particular victim is not saved already
    if (medkits_left >= medkits_needed_to_rescue) and (message["victim_id"] not in victimIDtable[player_room]):
        victimIDtable[player_room].add(message["victim_id"])
        victimIDtable[player_room +"med_kits"][0] = medkits_left - medkits_needed_to_rescue
        if medkits_needed_to_rescue==2:
            earnings[player_room][message["idx"]]["ind"] += 0.3
        else:
            earnings[player_room]['team'] += 0.1

        new_message["key"] = "rescue_success"
        new_message["medkits_used"] = medkits_needed_to_rescue # for victim
        new_message["medkits_left_team" ] = medkits_left - medkits_needed_to_rescue
        new_message["vics_left_team"] = victimIDtable[player_room +"med_kits"][1] - len(victimIDtable[player_room])
        await sio.emit("rescue_success", new_message, room=player_room)
        loop = asyncio.get_event_loop()
        loop.run_in_executor(executor, save_data, player_room, sid, new_message)

    elif medkits_left < medkits_needed_to_rescue:
        # new_message = message
        new_message["key"] = "rescue_fail"
        new_message["medkits_needed"] = medkits_needed_to_rescue
        new_message["medkits_left_team" ] = medkits_left
        new_message["vics_left_team"] = victimIDtable[player_room +"med_kits"][1] - len(victimIDtable[player_room])

        console.print("ERROR: in RESCUE SUCCESS, not enough medkits ", sid, style="bold red")
        await sio.emit("rescue_failure", new_message, room=player_room)
        loop = asyncio.get_event_loop()
        loop.run_in_executor(executor, save_data, player_room, sid, new_message)

    else:
        console.print("ERROR: in RESCUE SUCCESS ", sid, style="bold red")
    
    # end game if all victims are saved
    if len(victimIDtable[player_room]) == victimIDtable[player_room +"med_kits"][1]: 
        gameTable[player_room] = False
        end_message = {"topic" : "mission_end",
                        "rm_id" : player_room,
                        "key": "all_victims_saved", 
                        "publisher" : "api",
                        "time": datetime.now().isoformat()}
        await sio.emit('end_game', end_message, room=player_room)
        loop = asyncio.get_event_loop()
        loop.run_in_executor(executor, save_data, player_room, sid, end_message)
        loop.run_in_executor(executor, save_data, player_room, "earnings", earnings[player_room])

    elif victimIDtable[player_room +"med_kits"][0] ==0: # end game if all medkits are used 
        gameTable[player_room] = False
        end_message = {"topic" : "mission_end",
                        "rm_id" : player_room,
                        "key": "all_medkits_used", 
                        "publisher" : "api",
                        "time": datetime.now().isoformat()}
        await sio.emit('end_game', end_message, room=player_room)
        loop = asyncio.get_event_loop()
        loop.run_in_executor(executor, save_data, player_room, sid, end_message)
        loop.run_in_executor(executor, save_data, player_room, "earnings", earnings[player_room])


# client sends teammate update request
@sio.on("teammate_update_request")
async def teammate_update_request_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    # console.print(message, sid, style="bold blue")
    session      = await sio.get_session(sid)
    player_room  = session['room_id']
    await sio.emit("player_update_requested", message, room=player_room)
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)
    
# client sends teammate update form submission
@sio.on("teammate_request_submission")
async def teammate_update_request_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    # console.print(message, sid, style="bold blue")
    session      = await sio.get_session(sid)
    player_room  = session['room_id']
    await sio.emit("teammate_update_received", message, room=player_room)
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)

# client sends teammate note msg form submission
@sio.on("teammate_msg_submission")
async def teammate_update_request_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    # console.print(message, sid, style="bold blue")
    session      = await sio.get_session(sid)
    player_room  = session['room_id']
    await sio.emit("teammate_msg_received", message, room=player_room)
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)

# client sends end game when timer is done
@sio.on("end_game")
async def end_game_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    session             = await sio.get_session(sid)
    try:
        player_room         = session['room_id']
    except KeyError:
        player_room = message['rm_id']
    gameTable[player_room] = False
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)
    loop.run_in_executor(executor, save_data, player_room, "earnings", earnings[player_room])

# client sends end survey and data is stored
@sio.on("end_game_survey")
async def end_survey_handler(sid, message):
    message["time"] = datetime.now().isoformat()
    session             = await sio.get_session(sid)
    try:
        player_room         = session['room_id']
    except KeyError:
        player_room = message['rm_id']
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)


# default
@sio.on("disconnect")
async def disconnect_handler(sid):
    try:
        session             = await sio.get_session(sid)
        player_room         = session['room_id']
        room_read_count_str = str(player_room)+"_ready_count"
        current_count       = int(r.get(room_read_count_str))
        current_room        = r.get("room_id")
    except KeyError:
        # connect then disconnect
        console.print("DISCONNECTED: player never was assigned to a room", sid, style="bold red")
        return
        
    message = {
        "topic" : "disconnect",
        "key" : "disconnected",
        "rm_id" : player_room,
        "publisher" : "api",
        "time" : datetime.now().isoformat()
    }
    try:
        if gameTable[player_room]:
            message["key"] = "disconnected_during_game"
            await sio.emit("participant_left", {"msg":"End Game!"}, room=player_room)
            console.print("DISCONNECTED player during game", sid, style="bold red")

        else:
             # we don't want the page to refresh while participants are filling out post surveys
             message["key"] = "disconnected_postsurveys"
             console.print("DISCONNECTED player during/after post surveys", sid, style="bold red")

    except KeyError:
        # disconnected while waiting on teammate
        message["key"] = "disconnected_waiting"
        console.print("DISCONNECTED player while waiting for teammate", sid, style="bold red")
        # disconnected player was in current room 
        r.decrby("n_player", 1)
    
    loop = asyncio.get_event_loop()
    loop.run_in_executor(executor, save_data, player_room, sid, message)
    



if __name__ == "__main__":

    key  = None
    cert = None
    if not os.getenv('NO_SSL'):
        key  = os.environ['SSL_KEY']
        cert = os.environ['SSL_CERT']

    uvicorn.run(app, host="0.0.0.0", port=8084, ssl_keyfile=key, ssl_certfile=cert)

